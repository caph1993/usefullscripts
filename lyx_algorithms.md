Paste the following at the end of Document -> Settings -> Local layout.
```
InsetLayout "Flex:algorithm"
    FreeSpacing      1
    KeepEmpty        1
    MultiPar         1
    LyXType          Custom
    LabelString      algorithm
    LatexType        environment
    LatexName        myAlgorithm
    Decoration       classic
    LabelFont
        Color        blue
        Size         Small
    EndFont
  Argument 1
    Mandatory 1
    AutoInsert 1
    LabelString "caption"
    Decoration conglomerate
  EndArgument
  Preamble
  \usepackage[ruled,linesnumbered]{algorithm2e}
  \newenvironment{myAlgorithm}[1]{
    \begin{algorithm}
    \DontPrintSemicolon
    \caption{#1}
  }{
    \end{algorithm}
  }
  EndPreamble
End


InsetLayout "Flex:indentedBox"
  LyxType     Custom
  LatexType   Command
  LatexName   "algoIndent"
  Decoration  minimalistic
  LabelType   Manual
  Preamble
    \SetKwProg{myCodeBlock}{}{}{}
    \newcommand{\algoIndent}[1]{\myCodeBlock{}{#1}}
  EndPreamble
End


Style "indentedStyle"
    Margin Static
    FreeSpacing 1
    KeepEmpty 1
    LeftMargin "MM"
    ParagraphGroup 1
End
```
# Usage
Start with insert -> Custom Insets -> algorithm.
Write the caption optionally, exit the caption but stay inside algorithm and write the algorithm.
For indenting, insert -> Custom Insets -> indentedBox, and immediately select indentedStyle from the top-left dropdown that usually says "Plain Layout".

The shortcut for the last part is discussed at: https://tex.stackexchange.com/questions/79277/what-is-the-shortcut-for-triggering-environments-in-lyx-2-0-4-without-having-to
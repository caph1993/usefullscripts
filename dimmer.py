from __future__ import print_function,division
import sys, subprocess, os

# Get the current path:
path,filename = os.path.split(os.path.realpath(__file__))

def run(cmd):
	return subprocess.check_output(cmd,shell=True,stderr=subprocess.STDOUT)

def save(arr):
	with open(path+'/.dimmer.txt','w') as f:
		f.write(' '.join([str(x) for x in arr]))
	return

def raw_load():
	with open(path+'/.dimmer.txt') as f:
		arr = [int(x) for x in f.readline().split()]
	return arr

def load():
	try:
		return raw_load()
	except:
		save([100,0])
		return raw_load()

def is_num(s):
	try: int(s); ret=True
	except: ret=False
	return ret

def usage():
	print("Usage:")
	print(" 1. python dimmer.py +5\tIncreases brightness")
	print(" 2. python dimmer.py -5\tDecreases brightness")
	print(" 3. python dimmer.py 70\tSets brightness")
	print(" 4. python dimmer.py get\tPrints brightness")
	print(" 5. python dimmer.py red\tToogles redshift mode")
	print("Brightness values are in percentage [20,100].")

# Notation:
#  x: brightness value (int)
#  r: redshift toogle (int % 3)

def set_brightness(x,r):
	x = max(20,min(100, x)) # Prevent shit to happen
	r = r % 3 # Just in case user is manipulating .dimmer.txt
	if r:
		# Redshift mode:
		def cmd():
			run('redshift -o -O %dK -b %f:%f'%(65e2-1e3*r, x/1e2,x/1e2))
		try:
			cmd()
		except:
			run('notify-send "Dimmer app" "Redshift installation required"')
			run('gksudo apt-get install redshift')
			cmd()
	else:
		# Normal mode:
		def cmd():
			disp = run('xrandr | grep " connected"').split()[0].decode()
			run('xrandr --output %s --brightness %f'%(disp,x/1e2))
		try:
			cmd()
		except:
			run('notify-send "Dimmer app" "xrandr installation required"')
			run('gksudo apt-get install xrandr')
			cmd()
	save([x,r])

def main():
	param = sys.argv[-1].strip()
	if param=='get':
		print(load()[0])
	elif param=='red':
		x, r = load()
		set_brightness(x, (r+1)%3)
	elif is_num(param):
		x, r = load()
		x = int(param) + (x if param[0] in ['+','-'] else 0)
		set_brightness(x, r)
	else:
		usage()

main()

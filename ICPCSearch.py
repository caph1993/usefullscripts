#! /bin/python3
from __future__ import print_function
import subprocess,sys

# Basic system call:
def run(cmd):
	return subprocess.check_output(cmd,shell=True,stderr=subprocess.STDOUT)

# Check dependencies:
pckgs = []
try: run('python3 -c "import google"')
except: pckgs.append("google")
try: run('python3 -c "import unicodedata"')
except: pckgs.append("unicodedata")
try: run('python3 -c "import re"')
except: pckgs.append("re")
try: run('python3 -c "import heapq"')
except: pckgs.append("heapq")
try: run('python3 -c "import os"')
except: pckgs.append("os")
if pckgs:
	print("[ERROR] Missing dependencies:\n")
	print("Please run sudo pip3 install "+' '.join(pckgs))
	exit(0)

import google,os,heapq,unicodedata,re

# Get the current path:
path,filename = os.path.split(os.path.realpath(__file__))

# Parse the input:
def main():
	global data
	if sys.argv[-1].strip() in ['--update','-u']:
		update()
		exit(0)
	if len(sys.argv)<=1 or sys.argv[-1].strip() in ['--help','-h']:
		print("Usage:")
		print(" 0. python3 ICPCSearch.py --help (or -h)")
		print("     + Displays this message")
		print(" 1. python3 ICPCSearch.py --update (or -u)")
		print("     + Updates the local copy of ICPC Problems Databse")
		print(" 2. python3 ICPCSearch.py \"Problem Name\"")
		print("     + Searches the given name")
		print("     + You may omit the quotations if desired ")
		print(" 3. python3 ICPCSearch.py Number")
		print("     + Searches the given number")
		print(" 4. python3 ICPCSearch.py [Name or Number] -n 13")
		print("     + Prints at most 13 results")
		print("     + Default is 5 results")
		exit(0)
	
	n = 5
	for i in range(len(sys.argv)):
		if sys.argv[i]=='-n':
			if i+1==len(sys.argv):
				print("Maximum number of results not specified.")
				exit(0)
			else:
				try:
					n = int(sys.argv[i+1])
					sys.argv[:] = sys.argv[:i]+sys.argv[i+1:]
					break
				except:
					print("Maximum number of results: expected a number.")
					exit(0)	
	n = max(1,n)
	name = ' '.join(x.strip() for x in sys.argv[1:])
	if name[0]==""==name[-1]: name = name[1:-1]
	data = load()
	try:
		num=int(name)
		if num in data:
			print("  Problem %d: %s"%(num,data[num]))
		else:
			print("Not found")
		exit(0)
	except:
		pass
	
	best = search(raw(name).strip(),n)
	
	if len(best)==1:
		d,num = best[0]
		print("  Problem %d: %s"%(num,data[num]))
	else:
		for i,(d,num) in enumerate(best):
			if i==0:
				print("Closest result:")
				print("  Problem %d: %s"%(num,data[num]))
				print("Other results:")
			else:
				print("  %2d. Problem %d: %s"%(i,num,data[num]))

# fuzzy Search function:
def search(s,n=5):
	# n: Number of results to print
	s = raw(s).strip()
	hq = [] # Heap with closest results
	forced = set() # Set with results that contain input as a substring
	for num in data:
		x = raw(data[num]).strip()
		d = strDist(s,x)
		if(len(hq)<n or d>hq[0][0]):
			heapq.heappush(hq,(d,num))
			if(len(hq)>n):
				heapq.heappop(hq)
	hq.sort()
	hq.reverse()
	return hq

# Update local copy from uHunt API:
def update():
	print("Updating...")
	y = eval(google.get_page("https://icpcarchive.ecs.baylor.edu/uhunt/api/p").decode())
	y = dict((u[1],u[2]) for u in y)
	with open(path+"/ICPCDatabase.txt","w") as File:
		File.write(str(y))
	print("Success!")
		
# Load Database from local copy:
def load():
	try:
		with open(path+"/ICPCDatabase.txt") as File:
			x = eval(File.readline())
		return x
	except:
		print("The local database was not found, or is corrupted.")
		print("Fixing...")
		update()
		return load()

# String Normalizer
def raw(x):
	x = unicodedata.normalize('NFD', x)
	x = x.encode('ascii', 'ignore')
	return x.decode().lower()
	
# Substring likelihood:
def strCov(a,b):
	dp = [[(0,0)]*(1+len(a)) for i in range(2)]
	r = 0
	for j in range(len(b)-1,-1,-1):
		r=1-r
		dp[r][len(a)] = (0,0)
		for i in range(len(a)-1,-1,-1):
			dp[r][i] = (0,0)
			p,n = dp[r][i+1]
			dp[r][i] = max(dp[r][i], (p,max(n-2,0)))
			p,n = dp[1-r][i]
			dp[r][i] = max(dp[r][i], (p,max(n-2,0)))
			if a[i]==b[j]!=' ':
				p,n = dp[1-r][i+1]
				dp[r][i] = max(dp[r][i], (p+1+n*2,n+1))
	p,n = dp[r][0]
	return p

# Str distance:
def strDist(a,b):
	for c in ['+','-','_','?','!']:
		a = a.replace(c,' '+c+' ')
		b = b.replace(c,' '+c+' ')
	ans = strCov(a,b)
	a = ' '.join(sorted(a.split()))
	b = ' '.join(sorted(b.split()))
	ans += strCov(a,b)
	return ans
	
	
if __name__=="__main__":
	main()

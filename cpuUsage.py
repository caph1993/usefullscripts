import subprocess

#	Requires:
#		sudo apt-get install sysstat 

procStat = subprocess.check_output("cat /proc/stat | grep 'cpu '",shell=True)
procStat = subprocess.check_output("mpstat 2 1 | grep 'Average'",shell=True)

l = []
for x in procStat.split():
	x = x.decode().replace(',','.')
	try: l.append(float(x))
	except: pass

if l:
	usage=100-l[-1]
	print("%.0f%%"%usage)
else:
	print("??")

#! /bin/python3
from __future__ import print_function
import subprocess

def run(cmd):
	return subprocess.check_output(cmd,shell=True,stderr=subprocess.STDOUT)

# Check dependencies:
print("Checking dependecies...",end="")
pckgs = []
try: run("git --help")
except: pckgs.append("git")
try: run("ssh -1246AaCfgKkMNnqsTtVvXxYy")
except: pckgs.append("ssh")
try: run("xsel -h")
except: pckgs.append("xsel")
if pckgs:
	print("[ERROR] Missing dependencies:\n")
	print("Please run sudo apt-get install "+' '.join(pckgs)+' -y')
	exit(0)
print("[OK] Already installed.")

# Generate ssh-keys if they don't exist:
print("Creating ssh-keys...",end="")
ids = [s.decode() for s in run("ls -a ~/.ssh").split()]
if('id_rsa' not in ids or 'id_rsa.pub' not in ids):
	# Create identities
	run("echo '\n\n\n' > tmpFile.txt")
	run("ssh-keygen < tmpFile.txt")
	run("rm tmpFile.txt")
	print("[OK] Done!")
else:
	print("[OK] Already created.")

# Check that the ssh Agent is running:
agentRunning = run("ps -e | grep [s]sh-agent")
if(not agentRunning.strip()):
	run("ssh-agent /bin/bash")

# Add the ssh-key:
run("ssh-add ~/.ssh/id_rsa")

# Copy to clipboard:
run("cat ~/.ssh/id_rsa.pub | xsel -pi")
run("cat ~/.ssh/id_rsa.pub | xsel -bi")

# Final user steps:
print("")
print("")
print("Please do the following:")
print("")
print(" Your ssh key has been copied to the clipboard.")
print(" The following steps help you to paste it in Bitbucket and")
print(" to make your first commit.")
print("")
print(" 1. From Bitbucket Cloud, choose avatar > Bitbucket settings")
print("    from the application menu.")
print("    The system displays the Account settings page.")
print(" 2. Click SSH keys.")
print("    The SSH Keys page displays. If you have any existing keys,")
print("    those appear on this page.")
print(" 3. Click Add Key.")
print("    Enter a Label for your new key, for example, home-PC.")
print("    Paste the contents of the clipboard in the Key field.")
print(" 4. Go to clone in your repo page and change HTTPS to SSH.")
print(" 5. Clone your repo:")
print("     + mkdir /path/to/your/project")
print("     + cd /path/to/your/project")
print("     + git init")
print("     + git remote add origin git@bitbucket.org:user/repo.git")
print(" 6. Make your first commit")
print("     + git config --global user.email \"you@example.com\"")
print("     + git config --global user.name \"Your Name\"")
print("     + echo \"Your Name\" >> contributors.txt")
print("     + git add contributors.txt")
print("     + git commit -m 'Initial commit with contributors'")
print("     + git push -u origin master")
print(" 7. Use git, e.g.")
print("     + git pull")
print("     + git add FileName(s)")
print("     + git status")
print("     + git commit -am \"commit message\"")
print("     + git push")


